package com.example.application

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        tryAgain()
    }
    private var isfirstPlayer: Boolean = true

    private fun init(){

        button00.setOnClickListener {
            checkPlayer(button00)
        }
        button01.setOnClickListener {
            checkPlayer(button01)
        }
        button02.setOnClickListener {
            checkPlayer(button02)
        }
        button10.setOnClickListener {
            checkPlayer(button10)
        }
        button11.setOnClickListener {
            checkPlayer(button11)
        }
        button12.setOnClickListener {
            checkPlayer(button12)
        }
        button20.setOnClickListener {
            checkPlayer(button20)
        }
        button21.setOnClickListener {
            checkPlayer(button21)
        }
        button22.setOnClickListener {
            checkPlayer(button22)
        }

    }
    private fun checkPlayer(button: Button){
        if (isfirstPlayer){
            button.text = "X"
            isfirstPlayer = !isfirstPlayer
            button.isClickable = false
        }else{
            button.text = "O"
            isfirstPlayer = !isfirstPlayer
            button.isClickable = false
        }
        checkWinner()
    }

    private fun checkWinner(){
        if (button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString() && emptyBox(button00)){
            Toast.makeText(this,"winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button11.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString() && emptyBox(button20)){
            Toast.makeText(this,"winner is ${button21.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString() && emptyBox(button10)){
            Toast.makeText(this,"winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button01.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString() && emptyBox(button12)){
            Toast.makeText(this,"winner is ${button02.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button02.text.toString()}", Toast.LENGTH_SHORT).show()
            interruptGame()
        }
        if (emptyBox(button00) && emptyBox(button01) && emptyBox(button02) && emptyBox(button10) && emptyBox(button11) && emptyBox(button12) && emptyBox(button20) && emptyBox(button21) && emptyBox(button22) ){
            Toast.makeText( this,"It is a Draw", Toast.LENGTH_SHORT).show()
        }
    }
    private fun tryAgain(){
        tryAgain.setOnClickListener {
            button00.text = ""
            button01.text = ""
            button02.text = ""
            button10.text = ""
            button11.text = ""
            button12.text = ""
            button20.text = ""
            button21.text = ""
            button22.text = ""
            button00.isClickable = true
            button01.isClickable = true
            button02.isClickable = true
            button10.isClickable = true
            button11.isClickable = true
            button12.isClickable = true
            button20.isClickable = true
            button21.isClickable = true
            button22.isClickable = true
            isfirstPlayer = true
        }
    }
    private fun interruptGame(){
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false
    }
    private fun emptyBox(button: Button): Boolean {
        return button.text.toString().isNotBlank()
     }
}